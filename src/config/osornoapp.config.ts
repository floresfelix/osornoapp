export const osornoappConfig = {
	email_contacto: "prensaimo@gmail.com",


	noticias_web_service: "https://www.municipalidadosorno.cl/app/noticias.php",
	dev_noticias_web_service: "http://localhost:8100/ws/noticias.php",

	base_url_noticias: "https://www.municipalidadosorno.cl/noticia.php?id=",
	base_url_noticia_imagen: "https://www.municipalidadosorno.cl/archivos/",

	eventos_web_service: "https://www.municipalidadosorno.cl/eventos.php",
	dev_eventos_web_service: "http://localhost:8100/ws/eventos.php",

	base_url_eventos: "https://www.municipalidadosorno.cl/evento.php?id=",
	base_url_evento_imagen: "https://www.municipalidadosorno.cl/archivos/",
	

	gmaps_api_key: "AIzaSyCcsLRSDWWaTzVCH63u4XdKCXJSI3g4Q4s",
};
