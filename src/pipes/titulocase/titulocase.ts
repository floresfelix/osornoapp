import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TitulocasePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
 @Pipe({
 	name: 'titulocase',
 })
 export class TitulocasePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
   transform(input:string, length: number): string{
   	return input.length > 0 ? input.replace(/\w\S*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() )) : '';
   }
}
