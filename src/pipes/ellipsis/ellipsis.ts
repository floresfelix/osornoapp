import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the EllipsisPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
 @Pipe({
 	name: 'ellipsis',
 })
 export class EllipsisPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
   transform(val, args) {
   	if (args === undefined) {
   		return val;
   	}

   	if (val.length > args) {
   		return val.substring(0, args) + '...';
   	} else {
   		return val;
   	}
   }
}
