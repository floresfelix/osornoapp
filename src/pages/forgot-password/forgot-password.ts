import { Component } from '@angular/core';
import { NavController, LoadingController,AlertController  } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';

import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login';
import { EmailValidator } from '../../components/validators/email.validator';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';



@Component({
  selector: 'forgot-password-page',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  forgot_password: FormGroup;
  main_page: { component: any };

  constructor(private firebaseAnalytics: FirebaseAnalytics,public nav: NavController, private afAuth: AngularFireAuth, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.main_page = { component: TabsNavigationPage };

    this.forgot_password = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, EmailValidator.isValid]))
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "forgot-password"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));


  }

  recoverPassword(){

    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    loading.present();


    console.log("Recover password");
    console.log(this.forgot_password.value);

    this.afAuth.auth.sendPasswordResetEmail(this.forgot_password.get("email").value).then((resolve) =>{
      this.nav.setRoot(LoginPage);
      loading.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Recuperar Contraseña',
        subTitle: 'Se ha enviado un mensaje al email ingresado con instrucciones para recuperar la contraseña.',
        buttons: ['Aceptar']
      });
      alert.present();

    },
    (reject) => {
      console.log(reject);
      loading.dismiss();
      if(reject.code == "auth/user-not-found"){
        let alert = this.alertCtrl.create({
          title: 'Recuperar Contraseña',
          subTitle: 'El email ingresado no está registrado.',
          buttons: ['Aceptar']
        });
        alert.present();
      }
      

    } );

  }

}
