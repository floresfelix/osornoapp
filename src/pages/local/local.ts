import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';
import { LocalModel } from './local.model';

@Component({
  selector: 'local-page',
  templateUrl: 'local.html'
})
export class LocalPage {
  contact: LocalModel = new LocalModel();

  constructor(
    public navCtrl: NavController,
    public inAppBrowser: InAppBrowser,
    private callNumber: CallNumber,
    private platform: Platform,
    ) {
  }



  //Note: we commented this method because the Call Number plugin was unstable and causing lots of errors. If you want to use it please go: https://ionicframework.com/docs/native/call-number/
  call(number: string){

    if(this.platform.is("cordova")){
      this.callNumber.callNumber(number, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    }
    else{
      console.log("no es cordova");

    }

    this.callNumber.callNumber(number, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));



  }


  navegar(address:string){
    let gmaps = "https://www.google.com/maps/search/?api=1&query=" + address;
    this.inAppBrowser.create(gmaps, '_system', "location=yes");

    

  }
  openInAppBrowser(website: string){
    this.inAppBrowser.create(website, '_system', "location=yes");
  }

}
