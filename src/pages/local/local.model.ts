export class LocalModel {
	images: Array<string> = [];
	name: string;
	rating: number;
	email: string;
	phone: string;
	website: string;
	address: string;
	horario: string;

	constructor() {
		this.images = [
		'./assets/images/maps/place-1.jpg',
		'./assets/images/maps/place-2.jpg',
		'./assets/images/maps/place-3.jpg',
		'./assets/images/maps/place-4.jpg'
		];
		this.name = "Cafe Popular";
		this.rating = 4;
		this.email = "contacto@osorno.com";
		this.phone = "555-555-555";
		this.website = "https://www.imo.cl";
		this.address = "Los Carrera 1231, Osorno";
		this.horario = "Abierto desde 8am a 6pm Lunes a Viernes";
	}
}
