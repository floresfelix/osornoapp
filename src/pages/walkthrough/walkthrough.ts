import { Component, ViewChild } from '@angular/core';
import { NavController, Slides,LoadingController, Events } from 'ionic-angular';
import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'walkthrough-page',
  templateUrl: 'walkthrough.html'
})
export class WalkthroughPage {
  loading: any;
  lastSlide = false;

  @ViewChild('slider') slider: Slides;

  constructor(public nav: NavController,
    public loadingCtrl: LoadingController,
    public authProvider: AuthProvider,
    public events: Events  ) {

  }

  skipIntro() {
    // You can skip to main app
    // this.nav.setRoot(TabsNavigationPage);

    // Or you can skip to last slide (login/signup slide)
    this.lastSlide = true;
    this.slider.slideTo(this.slider.length());
  }

  onSlideChanged() {
    // If it's the last slide, then hide the 'Skip' button on the header
    this.lastSlide = this.slider.isEnd();
  }

  goToLogin() {
    this.nav.push(LoginPage);
  }

  goToSignup() {
    this.nav.push(SignupPage);
  }

  doAnonymousLogin(){
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    this.loading.present();

    this.authProvider.anonymousLogin().then( (info) => {
      this.authProvider.authState = info;
      console.log(info);
      this.events.publish('user:login',info, Date.now());

      this.loading.dismiss();
      this.nav.setRoot(TabsNavigationPage);
    }).catch(function(error) {
      console.log(error)

    });
  }

}
