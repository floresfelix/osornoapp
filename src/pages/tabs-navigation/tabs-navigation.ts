import { Component } from '@angular/core';

import { EventosPage } from '../eventos/eventos';
import { ContactosPage } from '../contactos/contactos';

import {VideosPage} from '../videos/videos';
import {ComerPage} from '../comer/comer';
import {DormirPage} from '../dormir/dormir';


import {NoticiasPage} from '../noticias/noticias';
import {ReportePage} from '../reporte/reporte';
import {AuthProvider} from '../../providers/auth/auth';

@Component({
  selector: 'tabs-navigation',
  templateUrl: 'tabs-navigation.html'
})
export class TabsNavigationPage {

  pages: Array<{root: any, title: string, icon: string, class: string}>;

  constructor(public authProvider: AuthProvider) {

    this.pages = [];
    console.log(authProvider);
    if(authProvider.isUserAnonymousLoggedIn() == true){

      this.pages = [
      { root: NoticiasPage, title: 'Noticias', icon: "md-paper" , class: ""},
      { root: EventosPage, title: 'Eventos', icon: "md-calendar" , class: ""},
      { root: ContactosPage, title: 'Contacto', icon: "md-call" , class: ""},
      { root: VideosPage, title: 'Videos', icon: "logo-youtube" , class: ""},
      { root: ComerPage, title: 'Comer', icon: "md-restaurant" , class: ""},
      { root: DormirPage, title: 'Dormir', icon: "md-moon" , class: ""},



      ];


    }
    else{
      this.pages = [
      { root: NoticiasPage, title: 'Noticias', icon: "md-paper" , class: "" },
      { root: EventosPage, title: 'Eventos', icon: "md-calendar" , class: "" },
      { root: ReportePage, title: 'Reportar', icon: "md-warning" , class: "bigger-icon" },
      { root: ContactosPage, title: 'Contacto', icon: "md-call" , class: "" },
      { root: VideosPage, title: 'Videos', icon: "logo-youtube", class: "" }
      ];
    }


  }
}
