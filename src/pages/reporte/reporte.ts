import { Component } from '@angular/core';
import { NavController, SegmentButton, AlertController,LoadingController, ModalController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { counterRangeValidator } from '../../components/counter-input/counter-input';
//import { ImagePicker } from '@ionic-native/image-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';


//import { Crop } from '@ionic-native/crop';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Platform } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular'
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AuthProvider } from '../../providers/auth/auth';

import { MapModalPage } from '../map-modal/map-modal';

import * as firebase from 'firebase';

@Component({
  selector: 'reporte-page',
  templateUrl: 'reporte.html'
})
export class ReportePage {
  section: string;

  post_form: any;
  // event_form: FormGroup;
  // card_form: FormGroup;
  ubicacion:string = "";

  categories_checkbox_open: boolean;
  categories_checkbox_result;
  categories_checkbox_result_label;
  loading: any;
  selected_image: any;
  reportes:any[] = [];
  lat:number;
  lng:number;
  private CARPETA_IMAGENES:string = "img";
  current_user:string;
  current_user_email:string;

  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public loadingCtrl: LoadingController,
    private platform:Platform,
    private camera: Camera,
    public actionSheetCtrl: ActionSheetController,
    private afDatabase: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private firebaseAnalytics: FirebaseAnalytics,
    private modalCtrl: ModalController,
    public authProvider: AuthProvider

    ) {
    // this.current_user =  authProvider.getCurrentUserId();
    // this.current_user_email =  authProvider.getCurrentUserEmail();

    const authObserver = this.afAuth.authState.subscribe( user => {
      if (user) {
        this.current_user = user.uid;
        this.current_user_email = user.email;
        authObserver.unsubscribe();
      } else {
        this.current_user = "";
        authObserver.unsubscribe();
      }
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "reporte"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));

    this.section = "post";

    this.post_form = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),

    });

  }



  openMapModal(){
    var data = { message : 'hello world' };
    var modalPage = this.modalCtrl.create( MapModalPage ,data );
    modalPage.onDidDismiss(data => {
      console.log("cerrando modal");
      console.log(data);
      this.lat = data.lat;
      this.lng = data.lng;
      this.ubicacion = data.lugar;


    });
    modalPage.present();
  }


  limpiar_titulo(){
    this.post_form.controls['title'].setValue("");

  }

  limpiar_descripcion(){
    this.post_form.controls['description'].setValue("");

  }

  abrir_opciones() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Subir una imagen',
      buttons: [

      {
        text: 'Abrir la cámara',
        handler: () => {
          console.log('Abrir cámara');
          this.abrir_camara(1);

        }
      },

      {
        text: 'Ver la galería',
        handler: () => {
          console.log('ver galería');
          //this.openImagePicker();
          this.abrir_camara(0);

        }
      },
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });

    actionSheet.present();
  }



  abrir_camara(opcion){
    //opcion = 0 : PHOTOLIBRARY
    // opcion = 1 : camara


    const options: CameraOptions = {
      quality: 75,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      targetWidth: 1280,
      targetHeight: 1280,
      sourceType: opcion
    }

    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     //let base64Image =  imageData;

     this.selected_image = base64Image;
   }, (err) => {
     console.log("error:"  + err);

   });


  }

  localizar(){
    console.log("localizar");
    this.loading = this.loadingCtrl.create({
      content: "Geolocalizando..."
    });



    this.loading.present();
    this.geolocation.getCurrentPosition().then((resp) => {
       // resp.coords.latitude
     // resp.coords.longitude
     console.log("current location lat: " + resp.coords.latitude);
     console.log("current location lng: " + resp.coords.longitude);

     this.lat = resp.coords.latitude;
     this.lng = resp.coords.longitude;

     if(this.platform.is("cordova")){
       this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
       .then((result: NativeGeocoderReverseResult) => {
         console.log(JSON.stringify(result));
         this.ubicacion = result.thoroughfare + " " + result.subThoroughfare;

         this.loading.dismiss();

       })
       .catch( (error: any) => {
         console.log(error);
         this.loading.dismiss();
       } );

     }
     else{
       this.ubicacion = "lugar dummy";
       this.loading.dismiss();
     }





   }).catch((error) => {
     console.log('Error getting location', error);
     this.loading.dismiss();

     let alert = this.alertCtrl.create({
       title: 'Reporte',
       subTitle: 'Error al intentar geolocalizar.',
       buttons: ['Aceptar']
     });

     alert.present();

   });



 }


 createReporte(){
   console.log(this.post_form.value);
   console.log("current_user_email: "+ this.current_user_email);
   console.log("current_user: "+ this.current_user);

   var reporte:reporteSubir = {email: this.current_user_email,uuid: this.current_user , lat: this.lat, lng:this.lng, img: this.selected_image, titulo: this.post_form.get("title").value,  descripcion: this.post_form.get("description").value,categoria: this.categories_checkbox_result, ubicacion: this.ubicacion   };



   let loader = this.loadingCtrl.create({
     content: "Enviando..."
   });
   loader.present();



   this.cargar_imagen_firebase(reporte).then((url:string)=> {
     reporte.img = url;

     let key = this.afDatabase.list("/reportes").push( reporte ).key;
     reporte.$key = key;

     this.firebaseAnalytics.logEvent('report', {key: key})
     .then((res: any) => console.log(res))
     .catch((error: any) => console.error(error));

     loader.dismiss();

   },
   (error)=> {
     loader.dismiss();
     console.error("error al cargar imagen: "+ JSON.stringify(error));

   });




 }

 cargar_imagen_firebase(archivo:reporteSubir){



   let promesa = new Promise((resolve, reject) => {


     let storageRef = firebase.storage().ref();
     let nombreArchivo = new Date().valueOf();

     let uploadTask:firebase.storage.UploadTask = storageRef.child(`${ this.CARPETA_IMAGENES }/${ nombreArchivo } `).putString( archivo.img, 'data_url', {contentType: "image/jpeg"});


     uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
              (snapshot)=> {}, //saber el avance del archivo
              (error)=> {

                console.error("Eror al subir archivo: "+ JSON.stringify(error));
                let alert = this.alertCtrl.create({
                  title: 'Reporte',
                  subTitle: 'Error en el envío del reporte, por favor reintente.',
                  buttons: ['Aceptar']
                });
                alert.present();
                reject(error);
              }, //manejo de errors
              ()=> {

                let url= uploadTask.snapshot.downloadURL;
                let alert = this.alertCtrl.create({
                  title: 'Reporte',
                  subTitle: 'Gracias por ayudarnos a hacer nuestro trabajo, tu reporte fue enviado exitosamente.',
                  buttons: ['Aceptar']
                });

                alert.present();
                this.limpiar_formulario();
                resolve(url);


              });



   });

   return promesa;
 }



 limpiar_formulario(){
   this.post_form.controls['title'].setValue("");
   this.post_form.controls['description'].setValue("");
   this.post_form.controls['location'].setValue("");
   this.categories_checkbox_result = null;
   this.categories_checkbox_result_label = null;
   this.selected_image = null;


 }

 chooseCategory(){
   let alert = this.alertCtrl.create({
     cssClass: 'category-prompt',
     title: 'Tipo Reporte',
     subTitle: 'Escoja una categoría',
     buttons: [{
       text: 'Cancelar',
     },
     {
       text: 'Confirmar',
       handler: data => {

         this.categories_checkbox_open = false;
         this.categories_checkbox_result = data;
         this.categories_checkbox_result_label = data.split("_").join(" ");
       }
     }],

     inputs: [{
       type: 'radio',
       label: 'Luminarias',
       value: 'luminarias'
     },
     {
       type: 'radio',
       label: 'Calles en mal estado',
       value: 'calles_mal_estado'
     },
     {
       type: 'radio',
       label: 'Ripio camino',
       value: 'ripio_camino'
     },
     {
       type: 'radio',
       label: 'Construcciones',
       value: 'construcciones'
     },
     {
       type: 'radio',
       label: 'Semáfaros',
       value: 'semaforos'
     },
     {
       type: 'radio',
       label: 'Demarcaciones de calle',
       value: 'demarcaciones_calle'
     },
     {
       type: 'radio',
       label: 'Vallas peatonales',
       value: 'vallas_peatonales'
     },
     {
       type: 'radio',
       label: 'Señalética',
       value: 'señaletica'
     },
     {
       type: 'radio',
       label: 'Garita peatonal',
       value: 'garita_peatonal'
     },
     {
       type: 'radio',
       label: 'Microbasural',
       value: 'microbasural'
     },
     {
       type: 'radio',
       label: 'Contenedores de basura',
       value: 'contenedores_basura'
     },
     {
       type: 'radio',
       label: 'Pastizales',
       value: 'pastizales'
     },
     {
       type: 'radio',
       label: 'Poda de arboles',
       value: 'poda_arboles'
     },
     {
       type: 'radio',
       label: 'Aseo',
       value: 'aseo'
     }

     ]
   });






   alert.present().then(() => {
     this.categories_checkbox_open = true;
   });
 }




}


interface reporteSubir{
  $key?:string;
  titulo:string;
  descripcion:string;
  categoria:string;
  ubicacion:string;
  img:string;
  lat:number;
  lng:number;
  uuid:string;
  email:string;

}


