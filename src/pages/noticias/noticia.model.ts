export class NoticiaModel {
	id: string;
	str_titulo: string;
	str_archivohead: string;
	str_thumbs: string;
	cod_extension: string;
	str_observacion: string;
	fec_publicacion: string;
	fec_inicio: string;
	hor_inicio: string;
	fec_fin: string;
	hor_fin: string;

}
export class NoticiasModel {
	noticias: Array<NoticiaModel>;
}
