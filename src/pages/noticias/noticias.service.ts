import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular';

import 'rxjs/add/operator/toPromise';

import { NoticiasModel } from './noticia.model';
import {osornoappConfig} from "../../config/osornoapp.config";

@Injectable()
export class NoticiasService {
  web_service :string ="";

  constructor(public http: Http, private platform:Platform) {
    if(this.platform.is("cordova")){
      this.web_service = osornoappConfig.noticias_web_service;
    }
    else{
      this.web_service = osornoappConfig.dev_noticias_web_service;

    }
    
  }

  getData(pagina:number): Promise<NoticiasModel> {
    //return this.http.get('./assets/example_data/noticias.json')

    let url = this.web_service + '?pagina=' + pagina; 

    return this.http.get(url).timeout(20000)
    .toPromise()
    .then(response => response.json() as NoticiasModel)
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
