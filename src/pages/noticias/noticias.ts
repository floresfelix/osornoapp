import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

import 'rxjs/Rx';


import { NoticiasModel } from './noticia.model';
import { NoticiaPage } from '../noticia/noticia';

import { NoticiasService } from './noticias.service';
import { TitleCasePipe } from '@angular/common';
import {osornoappConfig} from "../../config/osornoapp.config";
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';



@Component({
  selector: 'noticias-page',
  templateUrl: 'noticias.html'
})
export class NoticiasPage {
  list1: NoticiasModel = new NoticiasModel();
  loading: any;
  pagina:number = 0;
  base_url_noticia_imagen:string = osornoappConfig.base_url_noticia_imagen;


  constructor(
    public nav: NavController,
    public noticiasService: NoticiasService,
    public loadingCtrl: LoadingController,
    private titlecasePipe:TitleCasePipe,
    private firebaseAnalytics: FirebaseAnalytics,
    private alertCtrl: AlertController
    ) {
    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "noticias"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));


  }

  ionViewDidLoad() {
    this.loading.present();
    this.noticiasService.getData(this.pagina).then(data => {
      this.list1.noticias = data.noticias;
      this.loading.dismiss();
    }).catch(error => {
      console.log(error);
      this.loading.dismiss();
      this.alerta_error();
    });
  }



  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    this.pagina = this.pagina + 1;
    setTimeout(() => {
      console.log(this.pagina);
      this.noticiasService.getData(this.pagina).then(data => {
        for (let i = 0; i < data.noticias.length; i++) {
          this.list1.noticias.push( data.noticias[i] );
        }


        console.log('Async operation has ended');
        infiniteScroll.complete();
        

      }).catch(error => {
        console.log(error);
        this.alerta_error();
      });


      infiniteScroll.complete();
    }, 500);









  }



  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.pagina = 0;

    this.noticiasService.getData(this.pagina).then(data => {
      this.list1.noticias = data.noticias;


    }).catch(error => {
      console.log(error);
      this.alerta_error();
    });


    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


  alerta_error(){
    let alert = this.alertCtrl.create({
      title: 'Noticias',
      message: 'Error al mostrar listado de noticias',
      buttons: ['Aceptar']
    });
    alert.present();
  }


  transformTitle(texto){
    return this.titlecasePipe.transform(texto);
  }


  goToNews(item: any) {
    this.nav.push(NoticiaPage, { noticia: item });
  }


}
