import { Component } from '@angular/core';
import { NavController, LoadingController,AlertController, Events  } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { SignupPage } from '../signup/signup';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { EmailValidator } from '../../components/validators/email.validator';
import { Facebook } from '@ionic-native/facebook';


import { AngularFireAuth} from 'angularfire2/auth';
import firebase from 'firebase';

import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AuthProvider } from '../../providers/auth/auth';



@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: FormGroup;
  main_page: { component: any };
  loading: any;


  constructor(
    private afAuth: AngularFireAuth,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public fb: Facebook,
    private firebaseAnalytics: FirebaseAnalytics,
    public authProvider: AuthProvider,
    public events: Events    ) {
    this.main_page = { component: TabsNavigationPage };

    this.login = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, EmailValidator.isValid])),
      password: new FormControl('', Validators.required)
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "login"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));

  }

  async doLogin(){

    console.log("dologin");
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    loading.present();


    try {
      const result = await this.authProvider.loginUser(this.login.get('email').value, this.login.get('password').value);
      if (result) {
        this.authProvider.authState = result;
        this.events.publish('user:login', result, Date.now());

        loading.dismiss();
        this.nav.setRoot(this.main_page.component);
      }  
    }
    catch (e) {
      loading.dismiss();
      console.error(e);

      if((e.code == "auth/wrong-password") || (e.code == "auth/user-not-found")){
        let alert = this.alertCtrl.create({
          title: 'Ingreso',
          subTitle: 'El email y/o contraseña ingresados son incorrectos, reintente por favor.',
          buttons: ['Aceptar']
        });
        alert.present();
      }



    }


  }


  doAnonymousLogin(){
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    this.loading.present();

    this.authProvider.anonymousLogin().then( (info) => {
      this.authProvider.authState = info;
      this.events.publish('user:login',info, Date.now());

      this.loading.dismiss();
      this.nav.setRoot(this.main_page.component);
    }).catch(function(error) {
      console.log(error)

    });


    // firebase.auth().signInAnonymously().then((info) => {
    //   console.log(info)
    //   this.loading.dismiss();
    //   this.nav.setRoot(this.main_page.component);
    // })
    // .catch(function(error) {
    //   console.log(error)

    // });
  }

  doFacebookLogin(){
    this.loading = this.loadingCtrl.create();

    this.fb.login(["email"]).then((loginResponse) => {
      let credential = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);

      // firebase.auth().signInWithCredential(credential).then((info) => {
        this.authProvider.loginFbUser(credential).then((info) => {
          this.authProvider.authState = info;
          this.events.publish('user:login', info, Date.now());
          this.loading.dismiss();
          this.nav.setRoot(this.main_page.component);
        }, (reject) => {
          console.log(reject);
          this.nav.setRoot(LoginPage);
          this.loading.dismiss();
        });
      });
  }


  // doFacebookLogin(){
  //   var provider = new firebase.auth.FacebookAuthProvider();


  //   firebase.auth().signInWithRedirect(provider).then(function() {
  //     return firebase.auth().getRedirectResult();
  //   }).then(function(result) {
  //     console.log(result);
  //     // This gives you a Facebook Access Token.
  //     // You can use it to access the Facebook API.
  //     var token = result.credential.accessToken;
  //     // The signed-in user info.
  //     var user = result.user;
  //     // ...
  //   }).catch(function(error) {
  //     console.log(error);
  //     // Handle Errors here.
  //     var errorCode = error.code;
  //     var errorMessage = error.message;
  //   });


  // }




  // doFacebookLogin() {
  //   this.loading = this.loadingCtrl.create();

  //   // Here we will check if the user is already logged in because we don't want to ask users to log in each time they open the app
  //   // let this = this;

  //   this.facebookLoginService.getFacebookUser()
  //   .then((data) => {
  //      // user is previously logged with FB and we have his data we will let him access the app
  //      this.nav.setRoot(this.main_page.component);
  //    }, (error) => {
  //     //we don't have the user data so we will ask him to log in
  //     this.facebookLoginService.doFacebookLogin()
  //     .then((res) => {
  //       this.loading.dismiss();
  //       this.nav.setRoot(this.main_page.component);
  //     }, (err) => {
  //       console.log("Facebook Login error", err);
  //     });
  //   });
  // }


  goToSignup() {
    this.nav.push(SignupPage);
  }

  goToForgotPassword() {
    this.nav.push(ForgotPasswordPage);
  }

}
