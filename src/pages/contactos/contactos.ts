import { Component } from '@angular/core';
import { NavController, LoadingController ,SegmentButton,Platform} from 'ionic-angular';

import 'rxjs/Rx';

import { ContactosModel } from './contactos.model';
import { ContactosService } from './contactos.service';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { CallNumber } from '@ionic-native/call-number';

import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
  selector: 'contactos-page',
  templateUrl: 'contactos.html'
})
export class ContactosPage {
  notifications: ContactosModel = new ContactosModel();
  loading: any;
  section: string;

  constructor(
    public nav: NavController,
    public contactosService: ContactosService,
    public loadingCtrl: LoadingController,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    private platform: Platform,
    private firebaseAnalytics: FirebaseAnalytics
    ) {

    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });
    this.section = "phones";


    this.firebaseAnalytics.logEvent('page_view', {page: "contactos"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));
  }

  ionViewDidLoad() {
    this.loading.present();
    this.contactosService
    .getData()
    .then(data => {
      this.notifications.today = data.today;
      this.notifications.social = data.social;
      this.notifications.educacion = data.educacion;
      this.notifications.salud = data.salud;
      this.notifications.emergencias = data.emergencias;
      this.notifications.aseo = data.aseo;
      this.notifications.justicia = data.justicia;
      this.notifications.contactos = data.contactos;
      this.notifications.otros = data.otros;


    });
  }

  ionViewDidEnter(){
    this.loading.dismiss();
  }



  ir(notification: any){
    this.iab.create("http://"+notification.message, "_system");


  }

  llamar(notification: any){

    if(this.platform.is("cordova")){
      this.callNumber.callNumber(notification.phone, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    }
    else{
      console.log("no es cordova");
    }
    

  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }


}
