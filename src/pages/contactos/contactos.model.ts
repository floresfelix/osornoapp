export class ContactoModel {
	phone: string;
	name: string;
	highlight: boolean;
}

export class ContactosModel {
	emergencias: Array<ContactoModel> = [];
	salud: Array<ContactoModel> = [];
	aseo: Array<ContactoModel> = [];
	educacion: Array<ContactoModel> = [];
	justicia: Array<ContactoModel> = [];
	today: Array<ContactoModel> = [];
	yesterday: Array<ContactoModel> = [];
	social: Array<ContactoModel> = [];
	contactos: Array<ContactoModel> = [];
	otros: Array<ContactoModel> = [];
}
