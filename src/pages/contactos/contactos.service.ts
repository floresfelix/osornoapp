import { Injectable } from "@angular/core";
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { ContactosModel } from './contactos.model';

@Injectable()
export class ContactosService {
  constructor(public http: Http) {}

  getData(): Promise<ContactosModel> {
    return this.http.get('./assets/example_data/contactos.json')
    .toPromise()
    .then(response => response.json() as ContactosModel)
    .catch(this.handleError);
  }





  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
