import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,AlertController } from 'ionic-angular';

import 'rxjs/Rx';

import { EventoModel } from '../eventos/eventos.model';
import { SocialSharing } from '@ionic-native/social-sharing';

import { TitleCasePipe } from '@angular/common';
import {osornoappConfig} from "../../config/osornoapp.config";
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
	selector: 'evento-page',
	templateUrl: 'evento.html'
})
export class EventoPage {
	evento: EventoModel = new EventoModel();
   titulo :string;
   loading: any;
   base_url_eventos :string = osornoappConfig.base_url_eventos;
   base_url_evento_imagen:string = osornoappConfig.base_url_evento_imagen;


   constructor(
      public nav: NavController,
      public navParams: NavParams,
      public loadingCtrl: LoadingController,
      public socialSharing: SocialSharing,
      private titlecasePipe:TitleCasePipe,
      private firebaseAnalytics: FirebaseAnalytics,
      public alertCtrl: AlertController
      ) {

      this.evento = this.navParams.get("evento");
      this.titulo = this.transformTitle(this.evento.str_actividad);
      this.loading = this.loadingCtrl.create();
      console.log(this.evento);

      this.firebaseAnalytics.logEvent('page_view', {page: "noticia"})
      .then((res: any) => console.log(res))
      .catch((error: any) => console.error(error));

      
   }

   splitTime(hora){
      return hora.split(":")[0] + ":" + hora.split(":")[1];

   }
   ionViewDidLoad() {
      this.loading.present();

   }

   ionViewDidEnter(){
      this.loading.dismiss();
   }

   sharePostFacebook(post) {
      this.loading = this.loadingCtrl.create();
      this.loading.present();
   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_eventos + post.id;
   this.socialSharing.shareViaFacebook(texto, post.image, null)
   .then(() => {
      this.loading.dismiss();
      console.log('Success!');
   })
   .catch(() => {
      this.loading.dismiss();

      let alert = this.alertCtrl.create({
         title: 'Facebook',
         subTitle: 'Error al intentar compartir.',
         buttons: ['Aceptar']
      });
      alert.present();
      console.log('Error');
   });
}

sharePostTwitter(post) {
   this.loading = this.loadingCtrl.create();
   this.loading.present();
   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_eventos + post.id;

   this.socialSharing.shareViaTwitter(texto, post.image, null)
   .then(() => {
      this.loading.dismiss();
      console.log('Success!');
   })
   .catch(() => {
      this.loading.dismiss();

      let alert = this.alertCtrl.create({
         title: 'Twitter',
         subTitle: 'Error al intentar compartir.',
         buttons: ['Aceptar']
      });
      alert.present();
      console.log('Error');
   });
}


transformTitle(texto){
   return this.titlecasePipe.transform(texto);
}




sharePostWhatsapp(post) {
   this.loading = this.loadingCtrl.create();
   this.loading.present();
   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_eventos + post.id;
   this.socialSharing.shareViaWhatsApp(texto, post.image, null)
   .then(() => {
      this.loading.dismiss();
      console.log('Success!');
   })
   .catch(() => {

      this.loading.dismiss();

      let alert = this.alertCtrl.create({
         title: 'WhatsApp',
         subTitle: 'Error al intentar compartir.',
         buttons: ['Aceptar']
      });
      alert.present();
      console.log('Error');
   });
}



}
