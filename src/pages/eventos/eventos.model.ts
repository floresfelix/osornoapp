export class EventoModel {
	id: string;
	str_actividad: string;
	str_lugar: string;
	fec_evento: string;
	hor_inicio: string;
	str_imagen: string;
	str_imagen2: string;
	cod_estadoEvento: string;
}

export class EventosModel {
	eventos: Array<EventoModel> = [];
}


