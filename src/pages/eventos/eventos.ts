import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController} from 'ionic-angular';

import 'rxjs/Rx';

import { EventosService } from './eventos.service';
import { EventoModel } from './eventos.model';
import { TitleCasePipe } from '@angular/common';
import { EventoPage } from "../evento/evento";

import {osornoappConfig} from "../../config/osornoapp.config";
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
  selector: 'eventos-page',
  templateUrl: 'eventos.html',
})
export class EventosPage {
  //listing: ListingModel = new ListingModel();
  eventos: Array<EventoModel> = [];
  vacio:boolean = true;
  loading: any;
  base_url_evento_imagen:string = osornoappConfig.base_url_evento_imagen;

  constructor(
    private titlecasePipe:TitleCasePipe,
    public nav: NavController,
    public eventosService: EventosService,
    public loadingCtrl: LoadingController,
    private firebaseAnalytics: FirebaseAnalytics,
    private alertCtrl: AlertController



    ) {
    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "eventos"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));



  }


  ionViewDidLoad() {
    this.loading.present();
    this.eventosService
    .getData()
    .then(data => {
      console.log(data.eventos);
      this.eventos = data.eventos;
      this.vacio = data.eventos.length == 0;
      this.loading.dismiss();
    }).catch(error => {
      console.log(JSON.stringify(error));
      this.loading.dismiss();
      this.alerta_error();
    });
  }





  alerta_error(){
    let alert = this.alertCtrl.create({
      title: 'Eventos',
      message: 'Error al mostrar listado de eventos',
      buttons: ['Aceptar']
    });
    alert.present();
  }



  doRefresh(refresher) {
    this.eventosService
    .getData()
    .then(data => {
      this.eventos = data.eventos;
      this.vacio = data.eventos.length == 0;

    }).catch(error => {
      console.log(JSON.stringify(error));

      this.alerta_error();
    });


    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }






  transformTitle(texto){
    return this.titlecasePipe.transform(texto);
  }

  splitTime(hora){
    return hora.split(":")[0] + ":" + hora.split(":")[1];

  }

  goToEvento(item: any) {
    this.nav.push(EventoPage, { evento: item});
  }



  // goToFeed(category: any) {
  //   console.log("Clicked goToFeed", category);
  //   this.nav.push(FeedPage, { category: category });
  // }

}
