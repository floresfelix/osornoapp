import { Injectable } from "@angular/core";
import { Http } from '@angular/http';
import { Platform } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';
import { EventosModel } from './eventos.model';
import {osornoappConfig} from "../../config/osornoapp.config";


@Injectable()
export class EventosService {

  web_service :string ="";


  constructor(public http: Http, private platform:Platform) {

    if(this.platform.is("cordova")){
      this.web_service = osornoappConfig.eventos_web_service;
    }
    else{
      this.web_service = osornoappConfig.dev_eventos_web_service;

    }


  }

  getData(): Promise<EventosModel> {
    return this.http.get(this.web_service).timeout(20000)
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
