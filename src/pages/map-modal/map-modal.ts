import { Component } from '@angular/core';
import { NavController, NavParams,ViewController, LoadingController,AlertController  } from 'ionic-angular';
import { Geolocation} from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';

import { Platform } from 'ionic-angular';
import { MouseEvent } from '@agm/core';
/*import { Keyboard } from '@ionic-native/keyboard';
*/import { Observable } from 'rxjs/Observable';

import { GoogleMap } from "../../components/google-map/google-map";
import { GoogleMapsService } from "../maps/maps.service";
import { MapsModel } from '../maps/maps.model';

/**
 * Generated class for the MapModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @Component({
 	selector: 'page-map-modal',
 	templateUrl: 'map-modal.html',
 })
 export class MapModalPage {

 	map_model: MapsModel = new MapsModel();

 	title: string = 'Plaza de Armas, Osorno';
 	lat: number = -40.5738362;
 	lng: number = -73.1357509;

 	zoom=13;
 	search_query: string = "";
 	loading: any;


 	// customStyle: any = [{
 	// 	stylers: [{
 	// 		"gamma": 0.83
 	// 	},
 	// 	{
 	// 		hue: "#1BA8E2"
 	// 	},
 	// 	{
 	// 		saturation: -20
 	// 	}
 	// 	]
 	// }, {
 	// 	featureType: "road",
 	// 	elementType: "geometry",
 	// 	stylers: [{
 	// 		lightness: 100
 	// 	},
 	// 	{
 	// 		visibility: "simplified"
 	// 	}
 	// 	]
 	// }, {
 	// 	featureType: "road",
 	// 	elementType: "labels",
 	// 	stylers: [{
 	// 		visibility: "off"
 	// 	}]
 	// }];

 	constructor(public navCtrl: NavController, 
 		public navParams: NavParams,
 		public viewCtrl : ViewController,
 		private loadingCtrl:LoadingController,
 		public geolocation: Geolocation,
 		private platform:Platform,
 		public alertCtrl: AlertController,
/* 		public keyboard: Keyboard,
*/ 		private nativeGeocoder: NativeGeocoder,
public GoogleMapsService: GoogleMapsService,


) {
 		this.geolocateMe();
 	}

 	ionViewDidLoad() {
 		console.log(this.navParams.get('message')); 

 	}



 	setMapLatLng(lat:number, lng:number, zoom:number){
 		this.lat = lat;
 		this.lng = lng;

 		if(zoom != null){
 			this.zoom = zoom;

 		}
 	}

 	selectSearchResult(place: google.maps.places.AutocompletePrediction){
 		this.search_query = place.description;
 		//env.map_model.search_query = place.description;
 		this.map_model.search_places_predictions = [];

 		this.GoogleMapsService.geocodePlace(place.place_id).subscribe(
 			place_location => {
 				this.setMapLatLng(place_location.lat(),place_location.lng(), 17)
 			},
 			e => {
 				console.log('onError: %s', e);
 			},
 			() => {
 				console.log('onCompleted');
 			}
 			);
 	}


 	search(query: string){
 		if(query !== "")
 		{
 			this.GoogleMapsService.getPlacePredictions(query).timeout(20000).subscribe(
 				places_predictions => {
 					this.map_model.search_places_predictions = places_predictions;
 				},
 				e => {
 					console.log('onError: %s', e);
 					let alert = this.alertCtrl.create({
 						title: 'Localización',
 						subTitle: 'Error al realizar la búsqueda.',
 						buttons: ['Aceptar']
 					});

 					alert.present();

 				},
 				() => {
 					console.log('onCompleted');
 				}
 				);
 		}else{
 			this.map_model.search_places_predictions = [];
 		}
 	}



 	clearSearch(){
 		let env = this;
/* 		this.keyboard.close();
*/ 	}

mapClicked($event: MouseEvent) {
	this.setMapLatLng($event.coords.lat,$event.coords.lng, null);
	this.reverseGeocode($event.coords.lat,$event.coords.lng);

}

reverseGeocode(lat:number, lng:number){
	this.search_query = "Localizando...";
	if(this.platform.is("cordova")){
		this.nativeGeocoder.reverseGeocode(lat, lng)
		.then((result: NativeGeocoderReverseResult) => {
			console.log(JSON.stringify(result));
			this.search_query = result.thoroughfare + " " + result.subThoroughfare;

		})
		.catch( (error: any) => {
			console.log(error);
		});
	}
	else{
		this.search_query = "lugar dummy";
	}
}

markerDragEnd(m: marker, $event: MouseEvent) {
	console.log('dragEnd', m, $event);
	this.setMapLatLng($event.coords.lat,$event.coords.lng, null);
	this.reverseGeocode($event.coords.lat,$event.coords.lng);



}




public closeModal(){
	this.viewCtrl.dismiss({lugar: this.search_query, lat: this.lat, lng: this.lng});
}




geolocateMe(){
	console.log("localizar");
	this.loading = this.loadingCtrl.create({
		content: "Geolocalizando..."
	});



	this.loading.present();
	this.geolocation.getCurrentPosition().then((resp) => {
		this.setMapLatLng(resp.coords.latitude,resp.coords.longitude, 17);


		if(this.platform.is("cordova")){
			this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude)
			.then((result: NativeGeocoderReverseResult) => {
				console.log(JSON.stringify(result));
				this.search_query = result.thoroughfare + " " + result.subThoroughfare;

				this.loading.dismiss();

			})
			.catch( (error: any) => {
				console.log(error);
				this.loading.dismiss();
			} );

		}
		else{
			this.search_query = "lugar dummy";
			this.loading.dismiss();
		}











	}).catch((error) => {
		console.log('Error getting location', error);
		this.loading.dismiss();

		let alert = this.alertCtrl.create({
			title: 'Reporte',
			subTitle: 'Error al intentar geolocalizar.',
			buttons: ['Aceptar']
		});

		alert.present();

	});



}
}


interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}