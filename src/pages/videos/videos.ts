import { Component } from '@angular/core';
import { MenuController, SegmentButton, App, NavParams, LoadingController,Platform } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

import { YtProvider } from './../../providers/yt/yt';
import { Observable } from 'rxjs/Observable';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';


import 'rxjs/Rx';

@Component({
  selector: 'videos-page',
  templateUrl: 'videos.html'
})
export class VideosPage {

  loading: any;
  videos: Observable<any[]>;
  playlistId = 'PLCAtCIdJD4_wSyiAfl5iU04Fj4SJI274o';

  constructor(
    public menu: MenuController,
    public app: App,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public socialSharing: SocialSharing,
    private ytProvider: YtProvider,
    private platform: Platform,
    private firebaseAnalytics: FirebaseAnalytics,
    private youtube: YoutubeVideoPlayer,

    ) {


    this.loading = this.loadingCtrl.create({
      content: "Cargando..."
    });
    
    this.videos = this.ytProvider.getListVideos(this.playlistId);

    this.firebaseAnalytics.logEvent('page_view', {page: "videos"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));

  }

  ionViewDidLoad() {
    this.loading.present();

  }


  ionViewDidEnter(){
    console.log("ionViewDidEnter");
    this.loading.dismiss();
  }

  openVideo(video) {
    if (this.platform.is('cordova')) {
      this.youtube.openVideo(video.snippet.resourceId.videoId);
    } else {
      window.open('https://www.youtube.com/watch?v=' + video.snippet.resourceId.videoId);
    }
  }



  sharePost(video) {
    console.log(video);

   //this code is to use the social sharing plugin
   // message, subject, file, url
   let url = "https://www.youtube.com/watch?v=" + video.snippet.resourceId.videoId;
   this.socialSharing.share(video.snippet.title, video.snippet.title, video.snippet.thumbnails.high.url, url)
   .then(() => {
     console.log('Success!');
   })
   .catch(() => {
     console.log('Error');
   });
 }

}
