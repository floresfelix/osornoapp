import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

@Component({
	selector: 'privacy-policy-page',
	templateUrl: 'privacy-policy.html'
})

export class PrivacyPolicyPage {

	constructor(public view: ViewController,private firebaseAnalytics: FirebaseAnalytics) {
		this.firebaseAnalytics.logEvent('page_view', {page: "privacy-policy"})
		.then((res: any) => console.log(res))
		.catch((error: any) => console.error(error));
	}

	dismiss() {
		this.view.dismiss();
	}
}
