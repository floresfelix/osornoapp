import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController,AlertController, Events } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import { LoginPage } from '../login/login';

import { Facebook } from '@ionic-native/facebook';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';

import { EmailValidator } from '../../components/validators/email.validator';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
  selector: 'signup-page',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: FormGroup;
  main_page: { component: any };
  loading: any;

  constructor(
    public nav: NavController,
    private afAuth: AngularFireAuth,
    public modal: ModalController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public fb: Facebook,
    private firebaseAnalytics: FirebaseAnalytics,
    public events: Events
    ) {
    this.main_page = { component: TabsNavigationPage };

    this.signup = new FormGroup({
      email: new FormControl('', Validators.compose([Validators.required, EmailValidator.isValid])),
      password: new FormControl('', Validators.required),
      password2: new FormControl('', Validators.required)

    });

    this.firebaseAnalytics.logEvent('page_view', {page: "signup"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));


  }

  completado_password(){
    return (this.signup.get('password').value.length) > 0 && (this.signup.get('password2').value.length > 0)
  }

  verifica_password(){

    let iguales = this.signup.get('password').value == this.signup.get('password2').value;
    
    return (this.completado_password() && iguales);
  }

  async doSignup() {
    console.log("dosignup");


    if (!this.signup.valid || !this.verifica_password()){
      return;
    }
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    loading.present();

    try {
      const result = await this.afAuth.auth.createUserWithEmailAndPassword(
        this.signup.get("email").value,
        this.signup.get("password").value
        );
      if (result) {
        console.log(result);
        loading.dismiss();
        this.events.publish('user:login', result, Date.now());

        this.nav.setRoot(this.main_page.component);
      }
    } catch (e) {
      loading.dismiss();
      console.error(e);
      console.log(e.code);
      

      if(e.code == "auth/email-already-in-use"){


        let alert = this.alertCtrl.create({
          title: 'Registro',
          subTitle: 'Este email ya tiene una cuenta creada, por favor ingrese uno diferente ó ingrese con su cuenta',
          buttons: [
          {
            text: 'Ingresar',
            handler: () => {
              console.log('ingresar');
              this.nav.push(LoginPage);
            }
          },
          {
            text: 'Reintentar',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
          
          ]
        });
        alert.present();
      }
      if(e.code == "auth/weak-password"){


        let alert = this.alertCtrl.create({
          title: 'Registro',
          subTitle: 'La contraseña debe tener a lo menos 6 caracteres.',
          buttons: ['Aceptar']
        });
        alert.present();
        this.signup.controls['password'].setValue("");
        this.signup.controls['password2'].setValue("");




      }

      

    }
  }
// doFacebookSignup() {
  //   this.loading = this.loadingCtrl.create();
  //   // Here we will check if the user is already logged in
  //   // because we don't want to ask users to log in each time they open the app
  //   let env = this;

  //   this.facebookLoginService.getFacebookUser()
  //   .then(function(data) {
  //      // user is previously logged with FB and we have his data we will let him access the app
  //      env.nav.setRoot(env.main_page.component);
  //    }, function(error){
  //     //we don't have the user data so we will ask him to log in
  //     env.facebookLoginService.doFacebookLogin()
  //     .then(function(res){
  //       env.loading.dismiss();
  //       env.nav.setRoot(env.main_page.component);
  //     }, function(err){
  //       console.log("Facebook Login error", err);
  //       env.loading.dismiss();
  //     });
  //   });
  // }



  doFacebookSignup(){
    this.loading = this.loadingCtrl.create();

    this.fb.login(["email"]).then((loginResponse) => {
      let credential = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);

      firebase.auth().signInWithCredential(credential).then((info) => {
        console.log(info);
        this.loading.dismiss();
        this.nav.setRoot(this.main_page.component);
      }, (reject) => {
        console.log(reject);
        this.nav.setRoot(SignupPage);
        this.loading.dismiss();
      });
    });



  }

  // doFacebookLogin(){
    //login no nativo
  //   var provider = new firebase.auth.FacebookAuthProvider();


  //   firebase.auth().signInWithRedirect(provider).then(function() {
  //     return firebase.auth().getRedirectResult();
  //   }).then(function(result) {
  //     console.log(result);
  //     // This gives you a Facebook Access Token.
  //     // You can use it to access the Facebook API.
  //     var token = result.credential.accessToken;
  //     // The signed-in user info.
  //     var user = result.user;
  //     // ...
  //   }).catch(function(error) {
  //     console.log(error);
  //     // Handle Errors here.
  //     var errorCode = error.code;
  //     var errorMessage = error.message;
  //   });




  


  // showTermsModal() {
  //   let modal = this.modal.create(TermsOfServicePage);
  //   modal.present();
  // }

  showPrivacyModal() {
    let modal = this.modal.create(PrivacyPolicyPage);
    modal.present();
  }

}
