import { Component } from '@angular/core';
import { NavController, SegmentButton, AlertController,LoadingController, ModalController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { Observable } from 'rxjs/Observable';


import { Platform } from 'ionic-angular';

import { ActionSheetController } from 'ionic-angular'
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
import { AuthProvider } from '../../providers/auth/auth';
import { SocialSharing } from '@ionic-native/social-sharing';
import {osornoappConfig} from "../../config/osornoapp.config";

import * as firebase from 'firebase';

@Component({
  selector: 'contacto-page',
  templateUrl: 'contacto.html'
})
export class ContactoPage {
  section: string;

  post_form: any;


  loading: any;

  current_user:string;
  current_user_email:string;

  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private platform:Platform,
    private afDatabase: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    private firebaseAnalytics: FirebaseAnalytics,
    private modalCtrl: ModalController,
    public authProvider: AuthProvider,
    private socialSharing: SocialSharing

    ) {

    const authObserver = this.afAuth.authState.subscribe( user => {
      if (user) {
        this.current_user = user.uid;
        this.current_user_email = user.email;
        authObserver.unsubscribe();
      } else {
        this.current_user = "";
        authObserver.unsubscribe();
      }
    });

    this.firebaseAnalytics.logEvent('page_view', {page: "contacto"})
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(error));

    this.section = "post";

    this.post_form = new FormGroup({
      title: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

  }


  enviarForm(){

    this.loading = this.loadingCtrl.create();
    this.loading.present();

   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description

   this.socialSharing.canShareViaEmail().then(() => {
     this.socialSharing.shareViaEmail(this.post_form.get("description").value,
       this.post_form.get("title").value,
       [osornoappConfig.email_contacto])
     .then(() => {
       this.loading.dismiss();
       console.log('Success!');
     })
     .catch(() => {
       this.loading.dismiss();
       console.log('Error');
       let alert = this.alertCtrl.create({
         title: 'Contacto',
         subTitle: 'Error al intentar enviar mensaje.',
         buttons: ['Aceptar']
       });
       alert.present();
     });

   }).catch(() => {
     this.loading.dismiss();
     console.log('Error: no puede enviar email');
     let alert = this.alertCtrl.create({
       title: 'Contacto',
       subTitle: 'Error: No hay correo configurado.',
       buttons: ['Aceptar']
     });
     alert.present();
   });




 }






 // enviarForm2(){
 //   console.log(this.post_form.value);
 //   console.log("current_user_email: "+ this.current_user_email);
 //   console.log("current_user: "+ this.current_user);


 //   if(this.platform.is("cordova")){
 //     this.emailComposer.isAvailable().then((available: boolean) =>{

 //       if(available) {
 //         console.log("emailComposer: isAvailable");
 //         let email = {
 //           app: 'mailto',
 //           to: osornoappConfig.email_contacto,
 //           subject: this.post_form.get("title").value,
 //           body: this.post_form.get("description").value,
 //           isHtml: true
 //         };

 //         this.emailComposer.open(email);

 //       }
 //       else{


 //         let alert = this.alertCtrl.create({
 //           title: 'Contacto',
 //           subTitle: 'Error en el envío del mensaje.',
 //           buttons: ['Aceptar']
 //         });
 //         alert.present();
 //       }
 //     });

 //   }
 //   else{
 //     console.log("No es cordova");
 //   }







 // }



}



