import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController,AlertController } from 'ionic-angular';

import 'rxjs/Rx';

import { NoticiaModel } from '../noticias/noticia.model';
import { SocialSharing } from '@ionic-native/social-sharing';

import { TitleCasePipe } from '@angular/common';
import {osornoappConfig} from "../../config/osornoapp.config";
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';


@Component({
	selector: 'noticia-page',
	templateUrl: 'noticia.html'
})
export class NoticiaPage {
	noticia: NoticiaModel = new NoticiaModel();
 titulo :string;
 loading: any;
 base_url_noticias :string = osornoappConfig.base_url_noticias;
 base_url_noticia_imagen:string = osornoappConfig.base_url_noticia_imagen;

 constructor(
  public nav: NavController,
  public navParams: NavParams,
  public loadingCtrl: LoadingController,
  public socialSharing: SocialSharing,
  private titlecasePipe:TitleCasePipe,
  private firebaseAnalytics: FirebaseAnalytics,
  public alertCtrl: AlertController
  ) {

  this.noticia = this.navParams.get("noticia");
  this.titulo = this.transformTitle(this.noticia.str_titulo);
  this.loading = this.loadingCtrl.create();


  this.firebaseAnalytics.logEvent('page_view', {page: "noticia"})
  .then((res: any) => console.log(res))
  .catch((error: any) => console.error(error));
}


ionViewDidLoad() {
  this.loading.present();

}

ionViewDidEnter(){
  this.loading.dismiss();
}

sharePostFacebook(post) {
  this.loading = this.loadingCtrl.create();
  this.loading.present();
   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_noticias + post.id;
   this.socialSharing.shareViaFacebook(texto, post.image, null)
   .then(() => {
    this.loading.dismiss();
    console.log('Success!');
  })
   .catch(() => {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
     title: 'Facebook',
     subTitle: 'Error al intentar compartir.',
     buttons: ['Aceptar']
   });
    alert.present();
    console.log('Error');

  });
 }

 sharePostEmail(post){
  this.loading = this.loadingCtrl.create();
  this.loading.present();



  // Check if sharing via email is supported
  this.socialSharing.canShareViaEmail().then(() => {

  // Sharing via email is possible
  console.log("es posible via email");
  let texto = this.base_url_noticias + post.id;
  this.loading.dismiss();
  // Share via email
  this.socialSharing.shareViaEmail(texto,  this.titulo, []).then(() => {
  // Success!
}).catch(() => {
 this.loading.dismiss();
  // Error!
});


}).catch(() => {
  this.loading.dismiss();
  // Sharing via email is not possible
  console.log("no es posible via email");
  let alert = this.alertCtrl.create({
   title: 'Email',
   subTitle: 'Error al intentar compartir vía email.',
   buttons: ['Aceptar']
 });
  alert.present();
});



}

sharePostTwitter(post) {
 this.loading = this.loadingCtrl.create();
 this.loading.present();

   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_noticias + post.id;

   this.socialSharing.shareViaTwitter(texto, post.image, null)
   .then(() => {
    this.loading.dismiss();
    console.log('Success!');
  })
   .catch(() => {
    this.loading.dismiss();
    console.log('Error');
    let alert = this.alertCtrl.create({
     title: 'Twitter',
     subTitle: 'Error al intentar compartir.',
     buttons: ['Aceptar']
   });
    alert.present();
  });
 }


 transformTitle(texto){
   return this.titlecasePipe.transform(texto);
 }




 sharePostWhatsapp(post) {
   this.loading = this.loadingCtrl.create();
   this.loading.present();
   //this code is to use the social sharing plugin
   // message, subject, file, url
   //let texto = post.name +": " + post.description
   let texto = this.base_url_noticias + post.id;
   this.socialSharing.shareViaWhatsApp(texto, post.image, null)
   .then(() => {
    this.loading.dismiss();
    console.log('Success!');
  })
   .catch(() => {
    this.loading.dismiss();

    console.log('Error');
    let alert = this.alertCtrl.create({
     title: 'Whatsapp',
     subTitle: 'Error al intentar compartir.',
     buttons: ['Aceptar']
   });
    alert.present();
  });
 }



}
