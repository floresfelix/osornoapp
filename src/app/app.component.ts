import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App,AlertController,Events } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Observable } from 'rxjs/Observable';

import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { LocalPage } from '../pages/local/local';
import { ComerPage } from '../pages/comer/comer';
import { DormirPage } from '../pages/dormir/dormir';

import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';

import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { LoginPage } from '../pages/login/login';
import { ContactoPage } from '../pages/contacto/contacto';



import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';
// import { Keyboard } from '@ionic-native/keyboard';
import {AuthProvider} from '../providers/auth/auth';

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any  ;

  pages: Array<{title: any, icon: string, component: any}>;

  constructor(
    platform: Platform,
    public menu: MenuController,
    private afAuth: AngularFireAuth,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public translate: TranslateService,
    private firebaseAnalytics: FirebaseAnalytics,
    public alertCtrl: AlertController,
    // public keyboard: Keyboard,
    public authProvider: AuthProvider,
    public events: Events
    ) {

    events.subscribe('user:login', (user, time) => {
    // user and time are the same arguments passed in `events.publish(user, time)`
    console.log('Welcome', user, 'at', time);
    console.log(this.authProvider.authState);

    var isAnonymous = user.isAnonymous;
    console.log("isAnonymous: "+ isAnonymous);
    this.rootPage = TabsNavigationPage;
    this.pages = [ { title: "Inicio", icon: 'home', component: TabsNavigationPage } ];


    if (isAnonymous){
      this.pages.push( { title: "Contacto", icon: 'mail', component: ContactoPage } );
    }
    else{
      this.pages.push(
        { title: "Donde Comer", icon: 'md-restaurant', component: ComerPage },
        { title: "Donde Dormir", icon: 'md-moon', component: DormirPage }
        );
    }

    this.pages.push({ title: "Privacidad", icon: 'paper', component: PrivacyPolicyPage })



    this.firebaseAnalytics.setUserId(user.uid)
    .then((res: any) => console.log(res))
    .catch((error: any) => console.error(JSON.stringify(error)));



  });
    // const authObserver = this.authProvider.authState.subscribe( user => {
    //   console.log("app.component");
    //   console.log(user);
    //   if (user) {
    //     var isAnonymous = user.isAnonymous;
    //     console.log("isAnonymous: "+ isAnonymous);
    //     this.rootPage = TabsNavigationPage;


    //     this.pages = [ { title: "Inicio", icon: 'home', component: TabsNavigationPage } ];

    //     if (isAnonymous){
    //       this.pages.push( { title: "Contacto", icon: 'mail', component: ContactoPage } );
    //     }
    //     else{
    //       this.pages.push(
    //         { title: "Donde Comer", icon: 'md-restaurant', component: ComerPage },
    //         { title: "Donde Dormir", icon: 'md-moon', component: DormirPage }
    //         );
    //     }

    //     this.pages.push({ title: "Privacidad", icon: 'paper', component: PrivacyPolicyPage })



    //     this.firebaseAnalytics.setUserId(user.uid)
    //     .then((res: any) => console.log(res))
    //     .catch((error: any) => console.error(JSON.stringify(error)));

    //     authObserver.unsubscribe();
    //   } else {
    //     this.rootPage = WalkthroughPage;

    //     authObserver.unsubscribe();
    //   }
    // });



    translate.setDefaultLang('es');
    translate.use('es');

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.splashScreen.hide();
      this.statusBar.styleLightContent();
      // this.keyboard.disableScroll(true);

      const authObserver = this.afAuth.authState.subscribe( user => {
        console.log("app.component");
        console.log(user);
        if (user) {
          var isAnonymous = user.isAnonymous;
          console.log("isAnonymous: "+ isAnonymous);
          this.rootPage = TabsNavigationPage;


          this.pages = [ { title: "Inicio", icon: 'home', component: TabsNavigationPage } ];

          if (isAnonymous){
            this.pages.push( { title: "Contacto", icon: 'mail', component: ContactoPage } );
          }
          else{
            this.pages.push(
              { title: "Donde Comer", icon: 'md-restaurant', component: ComerPage },
              { title: "Donde Dormir", icon: 'md-moon', component: DormirPage }
              );
          }

          this.pages.push({ title: "Privacidad", icon: 'paper', component: PrivacyPolicyPage })



          this.firebaseAnalytics.setUserId(user.uid)
          .then((res: any) => console.log(res))
          .catch((error: any) => console.error(JSON.stringify(error)));

          authObserver.unsubscribe();
        } else {
          this.rootPage = WalkthroughPage;

          authObserver.unsubscribe();
        }
      });



    });





  }


  logout(){
    console.log("logout");
    //this.facebookLoginService.doFacebookLogout();
    // this.afAuth.auth.signOut();
    this.authProvider.signOut();
    this.nav.setRoot(LoginPage);
    this.menu.close();


  }


  confirm_logout() {
    let alert = this.alertCtrl.create({
      title: 'Salir',
      message: '¿Está seguro que desea salir?',
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Salir',
        handler: () => {
          console.log('saliendo');
          this.logout();
        }
      }
      ]
    });
    alert.present();
  }



  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.nav.push(page.component);
  }
}
