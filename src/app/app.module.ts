import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ChangeDetectorRef } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';

import { TitleCasePipe } from '@angular/common'
import {TitulocasePipe} from "../pipes/titulocase/titulocase";
import {EllipsisPipe} from "../pipes/ellipsis/ellipsis";
import { TruncateModule } from 'ng2-truncate';


//paginas usadas
import { EventosPage } from '../pages/eventos/eventos';
import { EventoPage } from '../pages/evento/evento';
import { LocalPage } from '../pages/local/local';

import { NoticiasPage } from '../pages/noticias/noticias';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { VideosPage } from '../pages/videos/videos';
import { NoticiaPage } from "../pages/noticia/noticia";
import { ContactosPage } from '../pages/contactos/contactos';
import { ReportePage } from '../pages/reporte/reporte';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { MapModalPage } from '../pages/map-modal/map-modal';
import { ComerPage } from '../pages/comer/comer';
import { DormirPage } from '../pages/dormir/dormir';
import { ContactoPage } from '../pages/contacto/contacto';


import { GoogleMap } from '../components/google-map/google-map';
import { GoogleMapsService } from '../pages/maps/maps.service';
import { MapsPage } from '../pages/maps/maps';


//servicios usados
import { EventosService } from '../pages/eventos/eventos.service';
import { ContactosService } from '../pages/contactos/contactos.service';
import { NoticiasService } from '../pages/noticias/noticias.service';


//plugins usados
import { CallNumber } from '@ionic-native/call-number';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { YtProvider } from '../providers/yt/yt';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Camera } from '@ionic-native/camera';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { firebaseConfig } from "../config/firebase.config";
import { osornoappConfig } from "../config/osornoapp.config";


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { FirebaseAnalytics } from '@ionic-native/firebase-analytics';

import { AgmCoreModule } from '@agm/core';




//============================================

import { LoginPage } from '../pages/login/login';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { SignupPage } from '../pages/signup/signup';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';



import { PreloadImage } from '../components/preload-image/preload-image';
import { BackgroundImage } from '../components/background-image/background-image';
import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';


import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';


import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook } from '@ionic-native/facebook';
/*import { Keyboard } from '@ionic-native/keyboard';
*/



// Functionalities
import { ContactCardPage } from '../pages/contact-card/contact-card';

import { ValidatorsModule } from '../components/validators/validators.module';

import { LanguageService } from '../providers/language/language.service';
import { AuthProvider } from '../providers/auth/auth';





export function createTranslateLoader(http: Http) {
	return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
    declarations: [
    MyApp,
    EventosPage,
    EventoPage,
    LocalPage,
    LoginPage,
    NoticiaPage,
    ContactosPage,
    TabsNavigationPage,
    WalkthroughPage,
    SignupPage,
    ForgotPasswordPage,
    NoticiasPage,
    ContactoPage,
    MapModalPage,
    ReportePage,
    PrivacyPolicyPage,
    ContactCardPage,
    MapsPage,
    ComerPage,
    DormirPage,
    VideosPage,
    PreloadImage,
    BackgroundImage,
    ShowHideContainer,
    ShowHideInput,
    GoogleMap,
    TitulocasePipe,
    EllipsisPipe
    ],
    imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule,
    TruncateModule,
    IonicModule.forRoot(MyApp, {
        animate: true, // disable animation
        modalEnter: 'modal-slide-in',
        modalLeave: 'modal-slide-out',
        //pageTransition: 'ios-transition',
        swipeBackEnabled: false,
        backButtonText: "atrás"
    }),
    AgmCoreModule.forRoot({
        apiKey: osornoappConfig.gmaps_api_key
    }),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        }
    }),
    ValidatorsModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
    MyApp,
    EventosPage,
    EventoPage,
    LocalPage,
    LoginPage,
    ContactosPage,
    MapsPage,
    ContactoPage,
    MapModalPage,
    ComerPage,
    DormirPage,
    NoticiaPage,
    TabsNavigationPage,
    WalkthroughPage,
    ForgotPasswordPage,
    SignupPage,
    NoticiasPage,
    ReportePage,
    PrivacyPolicyPage,
    ContactCardPage,
    VideosPage
    
    ],
    providers: [
    AngularFireDatabase,
    EventosService,
    ContactosService,
    NoticiasService,
    CallNumber,
    YoutubeVideoPlayer,
    LanguageService,
    SplashScreen,
    StatusBar,
    SocialSharing,
    NativeStorage,
    Facebook,
    // Keyboard,
    GoogleMapsService,
    Geolocation,
    FirebaseAnalytics,
    NativeGeocoder,
    Camera,
    InAppBrowser,
    YtProvider,
    TitleCasePipe,
    AuthProvider
    ],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
