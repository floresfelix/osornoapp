import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
@Injectable()
export class AuthProvider {
  // public currentUser: Observable<firebase.User>;
  // public fireAuth: firebase.auth.Auth;
  public authState: any = null;

  constructor(private afAuth: AngularFireAuth) {

    const unsubscribe = this.afAuth.authState.subscribe( auth => {
      if (auth){
        this.authState = auth
        unsubscribe.unsubscribe();
      }
      else{
        this.authState = null;
        unsubscribe.unsubscribe();

      }
    });
  /*  const authObserver = this.afAuth.authState.subscribe( user => {
      if (user) {
        this.currentUser = user;
        var isAnonymous = user.isAnonymous;
        console.log("isAnonymous: "+ isAnonymous);

        authObserver.unsubscribe();
      } else {
        authObserver.unsubscribe();
      }
    });*/

    // this.fireAuth = firebase.auth();
    // firebase.auth().onAuthStateChanged(user => {
    //   this.currentUser = user;
    // });

  }

  isUserAnonymousLoggedIn() {
    return (this.authState !== null) ? this.authState.isAnonymous : false
  }

  // getUser(): Observable<firebase.User> {
  //   return this.currentUser;
  // }


  loginUser(email: string, password: string): Promise<any> {
    this.authState = null;
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  loginFbUser(credential:any): Promise<any>{
    this.authState = null;

    return firebase.auth().signInWithCredential(credential);
  }



  getCurrentUserId(): string {
    return (this.authState !== null) ? this.authState.uid : '';
  }

  getCurrentUserEmail(): string {
    return (this.authState !== null) ? this.authState.email : '';
  }

  anonymousLogin() {
    this.authState = null;

    //   return this.afAuth.auth.signInAnonymously()
    //   .then((user) => {
    //     console.log(user);
    //     this.authState = user
    //   })
    //   .catch(error => console.log(error));
    // 
    return this.afAuth.auth.signInAnonymously();
    

  }

  signOut(): void {
    this.afAuth.auth.signOut();
  }



  // createAnonymousUser(): Promise<any> {
  //   return  firebase.auth().signInAnonymously();

  // }



}