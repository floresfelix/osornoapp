import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the YtProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class YtProvider {
  	apiKey = 'AIzaSyAgFuX-YUnCuqSv9FmZndH49d_ZlWSRIU0';
  	constructor(public http: Http) { 
  		console.log('Hello YtProvider Provider');
  	}

  	getPlaylistsForChannel(channel) {
  		return this.http.get('https://www.googleapis.com/youtube/v3/playlists?key=' + this.apiKey + '&channelId=' + channel + '&part=snippet,id&maxResults=20')
  		.map((res) => {
        return res.json()['items'];
      })
  	}
  	
  	getListVideos(listId) {
  		return this.http.get('https://www.googleapis.com/youtube/v3/playlistItems?key=' + this.apiKey + '&playlistId=' + listId +'&part=snippet,id&maxResults=20')
  		.map((res) => {
        return res.json()['items'];
      })
  	}

  	

  }
